unit UMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.IniFiles,
  Data.DB, DBAccess, MemDS, System.Classes,
  Uni, UniProvider, MySQLUniProvider, OracleUniProvider, SQLServerUniProvider
  ;

type
  TFrmMain = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Memo1: TMemo;
    Label3: TLabel;
    TestConnBtn: TButton;
    UpgradeBtn: TButton;
    UpgradeConn: TUniConnection;
    QueryTemp: TUniQuery;
    procedure TestConnBtnClick(Sender: TObject);
    procedure UpgradeBtnClick(Sender: TObject);
  private
    FConnStr: string;
    function openDB(const AType: Word): Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

uses
  UDBUpgrade;

{$R *.dfm}

function TFrmMain.openDB(const AType: Word): Boolean;
var
  sHost, sPort, sDBName, sUName, sPwd: string;
  iniFile: TIniFile;
  iniFileName: string;
begin
  Result := False;
  //先读取配置文件获取数据连接
  iniFileName := ExtractFilePath(ParamStr(0));
  iniFileName := iniFileName + 'config\config.ini';
  iniFile := TIniFile.Create(iniFileName);
  sHost := iniFile.ReadString('DATABASE', 'host', '127.0.0.1');
  sPort := iniFile.ReadString('DATABASE', 'port', '1521');
  sDBName := iniFile.ReadString('DATABASE', 'DBName', 'orcl');
  sUName := iniFile.ReadString('DATABASE', 'username', 'system');
  sPwd := iniFile.ReadString('DATABASE', 'password', '123456');


  case AType of
    DBT_ORACLE://Oracle
      begin
//        FConnStr := Format(CT_ORACLE, [sPwd, sUName, sHost, sPort, sDBName]);
        UpgradeConn.ProviderName := 'Oracle';
        UpgradeConn.SpecificOptions.Values['Direct'] := 'True';
        UpgradeConn.Server := sHost + ':' + sPort + ':' + sDBName;
        UpgradeConn.Username := sUName;
        UpgradeConn.Password := sPwd;
        //只有MYSQL需要设置UseUniCode，其他数据库设置后会导致中文乱码 Modify on 2015/09/28 10:58:48 Author by Le  RAD Studio XE8
//        UpgradeConn.SpecificOptions.Values['UseUniCode'] := 'True';
      end;
    DBT_SQLSERVER://Sql server
      begin
//        FConnStr := Format(CT_SQLSERVER, [sHost, sDBName, sUName, sPwd]);
        UpgradeConn.ProviderName := 'SQL Server';
        UpgradeConn.Server := sHost + ',' + sPort;
        UpgradeConn.Port := StrToInt(sPort);
        UpgradeConn.Database := sDBName;
        UpgradeConn.Username := sUName;
        UpgradeConn.Password := sPwd;
//        UpgradeConn.SpecificOptions.Values['OLEDBPorvider'] := 'prDirect';

      end;
    DBT_ACCESS://ACCESS
      begin
//        FConnStr := Format(CT_ACCESS, [sHost]);
      end;
    DBT_MYSQL://My Sql
      begin
//        FConnStr := Format(CT_MYSQL, [sHost, sDBName, sUName, sPwd, sPort]);
        UpgradeConn.ProviderName := 'MySQL';
        UpgradeConn.SpecificOptions.Values['UseUniCode'] := 'True';
      end
  else
    FConnStr := '';
    Exit;
  end;

  try
    UpgradeConn.Close;
    //MYSQL
//    UpgradeConn.Server := sHost;
//    UpgradeConn.Port := StrToInt(sPort);
//    UpgradeConn.Username := sUName;
//    UpgradeConn.Password := sPwd;
//    UpgradeConn.Database := sDBName;

    //Oracle
//    UpgradeConn.Server := sHost + ':' + sPort + ':' + sDBName;
//    UpgradeConn.Username := sUName;
//    UpgradeConn.Password := sPwd;

    //SQL Server
//    UpgradeConn.Server := sHost + ',' + sPort;
//    UpgradeConn.Port := StrToInt(sPort);
//    UpgradeConn.Database := sDBName;
//    UpgradeConn.Username := sUName;
//    UpgradeConn.Password := sPwd;

    Memo1.Lines.Add('connStr:' + UpgradeConn.ConnectString);

    UpgradeConn.Open;


    Label1.Caption := '数据库连接正常';
    Result := True;
  except
    on E: Exception do
    begin
      Memo1.Lines.Add('数据库连接失败，错误信息:' + E.Message);
      //Label1.Caption := '数据库连接失败，错误信息:' + E.Message;
    end;
  end;
end;

procedure TFrmMain.TestConnBtnClick(Sender: TObject);
begin
  openDB(DBT_ORACLE);
end;

procedure TFrmMain.UpgradeBtnClick(Sender: TObject);
var
  DMDBUpgrade: TDBUpgrade;
begin
  //这里如果不初始化为nil，在Assigned判断时有可能出问题
  DMDBUpgrade := nil;
  try
    if not Assigned(DMDBUpgrade) then
      DMDBUpgrade := TDBUpgrade.Create(Self);
    //DBUpdateConnectString为你需要升级的数据库对应的ADOConnection连接串

    DMDBUpgrade.DBUpdateConnectString := UpgradeConn.ConnectString;
    //1:ORACLE 2:SQLSERVER 3:ACCESS 4:MYSQL(暂不支持其他类型)
    //DMDBUpgrade.DBType := DBT_ORACLE;
    DMDBUpgrade.DBType := DBT_ORACLE;
    //该属性不是必须，主要用于日志文件中显示主程序版本号
    DMDBUpgrade.Version := 'v1.0.0.1';
    //该属性不是必须，主要用于保存更新日志至数据库时标明更新者信息。不给值在调用DBUpdate.databaseUpdate时会自动初始化
    DMDBUpgrade.ComputerName := 'YourComputerName';
    //该属性不是必须主要用于保存更新日志至数据库时标明更新者信息。不给值在调用DBUpdate.databaseUpdate时会自动初始化
    DMDBUpgrade.ComputerIP := 'YourComputerIP';
    //该属性不是必须，主要用于显示数据库当前更新进度。不给值时不会有反馈给所有者
    DMDBUpgrade.LabProgressPrompt := Label2;
    //这个函数调用后就直接开始升级了
    DMDBUpgrade.databaseUpdate;
    Label1.Caption := '更新完毕';
  finally
    //记得释放资源哦(一定记得养成好习惯)
    if Assigned(DMDBUpgrade) then
      FreeAndNil(DMDBUpgrade);
  end;
end;

end.
